#!/usr/bin/python
# This file is part of Ansible
#
# Ansible is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Ansible is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Ansible.  If not, see <http://www.gnu.org/licenses/>.

DOCUMENTATION = '''
---
module: ec2_tag 
short_description: create and remove tag(s) to ec2 resources.
description:
    - Creates, removes and lists tags from any EC2 resource.  The resource is referenced by its resource id (e.g. an instance being i-XXXXXXX). It is designed to be used with complex args (tags), see the examples.  This module has a dependency on python-boto.
version_added: "1.3"
options:
  resource:
    description:
      - The EC2 resource id. 
    required: true
    default: null 
    aliases: []
  state:
    description:
      - Whether the tags should be present or absent on the resource. Use list to interrogate the tags of an instance.
    required: false
    default: present
    choices: ['present', 'absent', 'list']
    aliases: []
  region:
    description:
      - region in which the resource exists. 
    required: false
    default: null
    aliases: ['aws_region', 'ec2_region']

author: Lester Wade
extends_documentation_fragment: aws
'''

EXAMPLES = '''
# Basic example of adding tag(s)
tasks:
- name: tag a resource
  local_action: ec2_tag resource=vol-XXXXXX region=eu-west-1 state=present
  args:
    tags:
      Name: ubervol
      env: prod

# Playbook example of adding tag(s) to spawned instances
tasks:
- name: launch some instances
  local_action: ec2 keypair={{ keypair }} group={{ security_group }} instance_type={{ instance_type }} image={{ image_id }} wait=true region=eu-west-1
  register: ec2

- name: tag my launched instances
  local_action: ec2_tag resource={{ item.id }} region=eu-west-1 state=present
  with_items: ec2.instances
  args:
    tags:
      Name: webserver
      env: prod
'''

# Note: this module needs to be made idempotent. Possible solution is to use resource tags with the volumes.
# if state=present and it doesn't exist, create, tag and attach. 
# Check for state by looking for volume attachment with tag (and against block device mapping?).
# Would personally like to revisit this in May when Eucalyptus also has tagging support (3.3).

import sys
import time
import ast
import re


try:
  import boto.ec2
except ImportError:
  print "failed=True msg='boto required for this module'"
  sys.exit(1)

def main():
  argument_spec = ec2_argument_spec()
  argument_spec.update(dict(
          resource = dict(required=True),
          tags = dict(),
          state = dict(default='present', choices=['present', 'absent', 'list']),
      )
  )
  module = AnsibleModule(argument_spec=argument_spec)

  resource = module.params.get('resource')
  tags = {}
  vol = {}
  state = module.params.get('state')
  volume_id = resource

  ec2 = ec2_connect(module)

  try:
    vol = ec2.get_all_volumes(volume_ids=[volume_id])[0]
  except boto.exception.EC2ResponseError:
    module.fail_json(msg="No matching Volume found")

  #Find the related instance
  if vol.attach_data.instance_id:
    filters = { 'resource-id' : vol.attach_data.instance_id }
  else:
    module.fail_json(msg="No attachment info found")

  tags_of_instance = ec2.get_all_tags(filters=filters)

  if not tags_of_instance:
    module.fail_json(msg="No tags found in the instance %s." % (vol.attach_data.instance_id))

  tagdict = {}

  for tag in tags_of_instance:
    if not re.match('aws', tag.name):
      tagdict[tag.name] = tag.value
    tagdict['Name'] = vol.attach_data.device

  if state == 'present':
    tagger = ec2.create_tags([volume_id], tagdict)
    filters = { 'resource-id' : volume_id }
    gettags = ec2.get_all_tags(filters=filters)
    module.exit_json(msg="Tags %s created for resource %s." % (gettags,resource), changed=True)

  if state == 'list':
    module.exit_json(changed=False, **tagdict)
  sys.exit(0)



from ansible.module_utils.basic import *
from ansible.module_utils.ec2 import *

main()